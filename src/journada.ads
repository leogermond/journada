package Journada is

   type Level_T is (Trace, Debug, Info, Warning, Error);

   Level : Level_T := Info;
   Display_Time : Boolean := True;

   function Must_Display_Time return Boolean is (Display_Time);
   procedure Put_Log
     (L : Level_T;
      S : String;
      Display_Time : Boolean := Must_Display_Time;
      End_Line : Boolean := True);

   procedure Trace (S : String);
   procedure Debug (S : String);
   procedure Info (S : String);
   procedure Warning (S : String);
   procedure Error (S : String);

end Journada;
